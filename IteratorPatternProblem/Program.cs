﻿using System;

namespace IteratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iterator Pattern");

            var bank = new Bank();
            // Req1. Mostrar clientes
            foreach (var item in bank.Clients)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Hacked Data");
            bank.Clients.Add("Hacker");

            foreach (var item in bank.Clients)
            {
                Console.WriteLine(item);
            }
        }
    }
}
