using System.Collections.Generic;

namespace IteratorPattern
{
    public class Bank
    {
        private List<string> clients;
        public List<string> Clients {
            get => this.clients;
        }

        public Bank() {
            this.clients = new List<string>() {
                "Roberto", "Madepa", "Fino", "Daniel"
            };
        }
    }
}