﻿using System;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            Console.WriteLine("Strategy Pattern");

            var oriente = new Team("Oriente");
            var blooming = new Team("Blooming");

            var attack = new AttackStrategy();
            var defense = new DefenseStrategy();

            oriente.Play();
            blooming.Play();

            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("------------------");
                if (random.Next() % 2 == 0) {
                    oriente.Strategy = attack;
                    blooming.Strategy = defense;
                } else {
                    oriente.Strategy = defense;
                    blooming.Strategy = attack;
                }

                oriente.Play();
                blooming.Play();
            }
        }
    }
}
