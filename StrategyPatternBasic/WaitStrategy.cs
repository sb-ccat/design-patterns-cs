using System;

namespace StrategyPattern
{
    public class WaitStrategy : IStrategy
    {
        public void Play()
        {
            Console.WriteLine("Waiting...");
        }
    }
}