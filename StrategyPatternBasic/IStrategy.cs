namespace StrategyPattern
{
    public interface IStrategy
    {
         void Play();
    }
}