using System;

namespace StrategyPattern
{
    public class AttackStrategy : IStrategy
    {
        public void Play()
        {
            Console.WriteLine("Attack");
        }
    }
}