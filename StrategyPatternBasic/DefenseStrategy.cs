using System;

namespace StrategyPattern
{
    public class DefenseStrategy : IStrategy
    {
        public void Play()
        {
            Console.WriteLine("Defense");
        }
    }
}