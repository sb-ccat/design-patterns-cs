﻿using System;
using System.Collections.Generic;

namespace Introduction
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var countryNames = new List<string>() {
                "Bolivia", "Argentina", "Ecuador"
            };

            Console.WriteLine("=============ORIGINAL=======");
            Console.WriteLine(String.Join(", ", countryNames));

            countryNames.Sort();

            Console.WriteLine("=============SORTED=======");
            Console.WriteLine(String.Join(", ", countryNames));

            var countries = new List<Country> {
                new Country() {Name = "Bolivia", Area = 90},
                new Country() {Name = "Argentina", Area = 1055},
                new Country() {Name = "Uruguay", Area = 78}
            };

            Console.WriteLine("=============ORIGINAL=======");
            Console.WriteLine(String.Join("\n", countries));

            Console.WriteLine("=============SORTED BY AREA=======");
            countries.Sort();
            Console.WriteLine(String.Join("\n", countries));

            Console.WriteLine("=============SORTED BY NAME=======");
            countries.Sort(new NameComparer());
            Console.WriteLine(String.Join("\n", countries));
        }
    }
}
