using System;
using System.Collections.Generic;

namespace Introduction
{
    public class Country : IComparable<Country>
    {
        public string Name {get; set;}
        public int Area {get; set;}

        int IComparable<Country>.CompareTo(Country country)
        {
            if (Area > country.Area) return 1;
            if (Area < country.Area) return -1;
            return 0;
        }

        public override string ToString()
        {
            return String.Format("Name: {0} - Area: {1}", Name, Area);
        }
    }

    public class NameComparer : IComparer<Country>
    {
        int IComparer<Country>.Compare(Country x, Country y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}
