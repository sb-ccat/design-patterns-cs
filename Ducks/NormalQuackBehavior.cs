using System;

namespace Ducks
{
    public class NormalQuackBehavior : IQuackBehavior
    {
        public void Quack()
        {
            Console.WriteLine("Quack");
        }

    }
}