namespace Ducks
{
    public interface IFlyBehavior
    {
         void Fly();
    }
}