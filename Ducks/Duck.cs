using System;

namespace Ducks
{
    public class Duck
    {

        public IFlyBehavior FlyBehavior {get; set;}
        public IQuackBehavior QuackBehavior {get; set;}

        /*
        public Duck(IFlyBehavior flyBehavior, IQuackBehavior quackBehavior) {
            FlyBehavior = flyBehavior;
            QuackBehavior = quackBehavior;
        }
        */

        public void Swim() {
            Console.WriteLine("Swimming");
        }

        public virtual void Display() {
            Console.WriteLine("Paint Duck");
        }

        public virtual void Run() {
            Console.WriteLine();
            Console.WriteLine("====================");
            Display();
            Swim();
            FlyBehavior.Fly();
            QuackBehavior.Quack();
        }
    }
}