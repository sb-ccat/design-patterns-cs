using System;

namespace Ducks
{
    public class NormalFlyBehavior : IFlyBehavior
    {
        public void Fly()
        {
            Console.WriteLine("Flying");
        }
    }
}