using System;

namespace Ducks
{
    public class SqueezBehavior : IQuackBehavior
    {
        public void Quack()
        {
            Console.WriteLine("Squeez");
        }
    }
}