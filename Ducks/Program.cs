﻿using System;

namespace Ducks
{
    class Program
    {
        static void Main(string[] args)
        {
               //var mallardDuck = new Duck(new NormalFlyBehavior(), new NormalQuackBehavior());
              
               var mallardDuck = new Duck() {
                   FlyBehavior = new NormalFlyBehavior(),
                   QuackBehavior = new NormalQuackBehavior()
               };

               mallardDuck.Run();

               var rubberdDuck = new Duck() {
                   FlyBehavior = new NoFlyBehavior(),
                   QuackBehavior = new SqueezBehavior()
               };

               rubberdDuck.Run();

               var decoyDuck = new Duck() {
                   FlyBehavior = new NoFlyBehavior(),
                   QuackBehavior = new NoQuackBehavior()
               };
        }
    }
}
