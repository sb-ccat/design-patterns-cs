namespace Ducks
{
    public interface IQuackBehavior
    {
         void Quack();
    }
}