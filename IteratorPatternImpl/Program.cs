﻿using System;

namespace IteratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iterator Pattern");

            var bank = new Bank();
            var iterator = bank.createIterator();

            while(!iterator.isDone()) {
                Console.WriteLine(iterator.next());
            }
        }
    }
}
