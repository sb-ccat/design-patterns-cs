namespace IteratorPattern
{
    public interface IteratorInterface
    {
         string next();
         bool isDone(); // hasNext();
         string currentItem();
    }
}