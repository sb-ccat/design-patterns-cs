using System.Collections.Generic;

namespace IteratorPattern
{
    public class Bank : IAggregate
    {
        private List<string> clients;

        public Bank() {
            this.clients = new List<string>() {
                "Roberto", "Madepa", "Fino", "Daniel"
            };
        }

        public IteratorInterface createIterator()
        {
            return new ClientsIterator(this.clients);
        }
    }
}