using System.Collections.Generic;

namespace IteratorPattern
{
    //ConcreteIterator
    public class ClientsIterator : IteratorInterface
    {
        private List<string> clients;
        private int position;

        public ClientsIterator(List<string> clients) {
            this.clients = clients;
            this.position = 0;
        }
        
        public string currentItem()
        {
            return this.clients[this.position];
        }

        public bool isDone()
        {
            return this.position >= this.clients.Count;
        }

        public string next()
        {
            return this.clients[this.position++];
        }
    }
}