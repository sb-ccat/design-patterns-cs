namespace ObserverPattern
{
    public interface IObserverAlt
    {
         void notify();
    }
}