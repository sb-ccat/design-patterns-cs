﻿namespace GildedRoseKata
{
    public class DecrementStrategy : IStrategy
    {
        private int delta;

        public DecrementStrategy(int delta)
        {
            this.delta = delta;
        }

        public void UpdateQuality(Item item)
        {
            if (item.SellIn <= 0) this.delta *= 2;

            item.Quality -= this.delta;
            if (item.Quality < 0) item.Quality = 0;
        }
    }
}