﻿namespace GildedRoseKata
{
    public class ResetStrategy : IStrategy
    {
        public ResetStrategy()
        {
        }

        public void UpdateQuality(Item item)
        {
            item.Quality = 0;
        }
    }
}