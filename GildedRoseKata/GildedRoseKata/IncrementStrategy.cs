﻿namespace GildedRoseKata
{
    public class IncrementStrategy : IStrategy
    {
        private int delta;

        public IncrementStrategy(int delta)
        {
            this.delta = delta;
        }

        public void UpdateQuality(Item item)
        {
            item.Quality += this.delta;
            if (item.Quality > 50) item.Quality = 50;
        }
    }
}