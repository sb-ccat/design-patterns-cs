﻿using System.Collections.Generic;

namespace GildedRoseKata
{
    public class GildedRose
    {
        IList<Item> Items;

        public GildedRose()
        {
            this.Items = new List<Item>();
        }

        public void Add(Item item)
        {
            this.Items.Add(item);
        }

        public void UpdateQuality()
        {
            foreach (var item in Items)
            {
                var strategy = this.GetStrategy(item);
                strategy.UpdateQuality(item);
            }
        }

        private IStrategy GetStrategy(Item item)
        {
            if (item.Name.StartsWith("Aged")) return new IncrementStrategy(1);
            //if (item.Name.StartsWith("Sulfuras")) return new DecrementStrategy(0); //no change
            if (item.Name.StartsWith("Sulfuras")) return new NoUpdateStrategy();
            if (item.Name.StartsWith("Backstage")) return new BackstageStrategy();
            if (item.Name.StartsWith("Conjured")) return new DecrementStrategy(2);
            return new DecrementStrategy(1);
        }
    }
}
