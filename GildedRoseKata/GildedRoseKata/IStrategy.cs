﻿namespace GildedRoseKata
{
    public interface IStrategy
    {
        void UpdateQuality(Item item);
    }
}