﻿namespace GildedRoseKata
{
    public class BackstageStrategy : IStrategy
    {
        public void UpdateQuality(Item item)
        {
            IStrategy strategy = null;

            if (item.SellIn > 10)
            {
                strategy = new IncrementStrategy(1);
            }
            else if (item.SellIn > 5)
            {
                strategy = new IncrementStrategy(2);
            } else if (item.SellIn > 0)
            {
                strategy = new IncrementStrategy(3);
            }
            else
            {
                strategy = new ResetStrategy();
            }

            strategy.UpdateQuality(item);
        }
    }
}