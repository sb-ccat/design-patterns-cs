﻿using System;
using System.Collections.Generic;

namespace GildedRoseKata
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("OMGHAI!");
            
            var app = new GildedRose();
            app.Add(new Item {Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20});
            app.Add(new Item {Name = "Aged Brie", SellIn = 2, Quality = 0});
            app.Add(new Item {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7});
            app.Add(new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80});
            app.Add(new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 15,
                    Quality = 20
            });
            // Nuevo requerimiento:
            app.Add(new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 6});
            
            app.UpdateQuality();
        }
    }
}
