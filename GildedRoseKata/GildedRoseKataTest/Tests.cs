﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security;
using GildedRoseKata;
using Xunit;

namespace GildedRoseKataTest
{
    public class Tests
    {
        private readonly GildedRose gildedRose;

        public Tests()
        {
            this.gildedRose = new GildedRose();
        }

        [Fact]
        public void TestQualityNeverNegative()
        {
            var item = new Item {Name = "Test", SellIn = 1, Quality = 0};
            this.gildedRose.Add(item);

            gildedRose.UpdateQuality();

            Assert.Equal(0, item.Quality);
        }

        [Fact]
        public void TestDecreaseQualityNormalItem()
        {
            var item = new Item {Name = "Test normal", SellIn = 15, Quality = 5};
            this.gildedRose.Add(item);

            this.gildedRose.UpdateQuality();
            Assert.Equal(4, item.Quality);
        }

        [Fact]
        public void TestDecreaseDoubleQualityNormalItem()
        {
            var item = new Item {Name = "Item x01", SellIn = 0, Quality = 10};
            this.gildedRose.Add(item);

            this.gildedRose.UpdateQuality();
            Assert.Equal(8, item.Quality);
        }

        [Fact]
        public void TestIncreaseQualityItemAgedBried()
        {
            var item = new Item {Name = "Aged Brie", SellIn = 2, Quality = 0};
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            Assert.Equal(1, item.Quality);
        }

        [Fact]
        public void TestQualityNeverGreaterThan50()
        {
            var item = new Item {Name = "Aged Brie", SellIn = 5, Quality = 50};
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            Assert.Equal(50, item.Quality);
        }

        [Fact]
        public void TestQualitySulfurasNeverChanged()
        {
            var item = new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80};
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            Assert.Equal(80, item.Quality);
        }

        [Fact]
        public void TestQualityBackstageSellInGreaterThan10()
        {
            var item = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 15,
                Quality = 20
            };
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            Assert.Equal(21, item.Quality);
        }

        [Fact]
        public void TestQualityBackstageSellInLessThan10()
        {
            var item = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 10,
                Quality = 20
            };
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            Assert.Equal(22, item.Quality);
        }

        [Fact]
        public void TestQualityBackstageSellInLessThan5()
        {
            var item = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 4,
                Quality = 20
            };
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            Assert.Equal(23, item.Quality);
        }
        
        [Fact]
        public void TestQualityBackstageSellIn0()
        {
            var item = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 0,
                Quality = 20
            };
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            Assert.Equal(0, item.Quality);
        }

        [Fact]
        public void TestQualityConjured()
        {
            var item = new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 6};
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            
            Assert.Equal(4, item.Quality);
        }
        
        [Fact]
        public void TestQualityConjuredNeverNegative()
        {
            var item = new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 0};
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            
            Assert.Equal(0, item.Quality);
        }
        
        [Fact]
        public void TestQualityDecreaseWhenQuality2()
        {
            var item = new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 2};
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            
            Assert.Equal(0, item.Quality);
        }
        
        [Fact]
        public void TestQualityDecreaseWhenQuality1()
        {
            var item = new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 1};
            this.gildedRose.Add(item);
            this.gildedRose.UpdateQuality();
            
            Assert.Equal(0, item.Quality);
        }
        
        // SellIn Tests
        [Fact]
        public void TestSellInDecreased()
        {
            var item = new Item {Name = "Test", SellIn = 1, Quality = 0};
            this.gildedRose.Add(item);

            gildedRose.UpdateQuality();

            Assert.Equal(0, item.SellIn);
        }
    }
}