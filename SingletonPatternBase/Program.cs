﻿using System;

namespace SingletonPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Singleton Pattern");

            var sg1 = Singleton.Instance;
            var sg2 = Singleton.Instance;

            Console.WriteLine("SG1 = {0}", sg1);
            Console.WriteLine("SG2 = {0}", sg2);

            if (sg1 == sg2) {
              Console.WriteLine("Son Singleton");
            } 
        }
    }
}
