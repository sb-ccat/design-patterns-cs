namespace SingletonPattern
{
    public class Singleton
    {
      private int hashCode;
      private static Singleton instance;

      public static Singleton Instance {
        get {
          if (instance == null) {
            instance = new Singleton();
          }
          return instance;
        }
      }

      private Singleton() {
        this.hashCode = this.GetHashCode();
      }

      public override string ToString() {
        return "Singleton Code: " + this.hashCode;
      }
    }
}