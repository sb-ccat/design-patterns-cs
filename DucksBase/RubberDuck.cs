using System;

namespace Ducks
{
    public class RubberDuck : Duck
    {
        public override void Display() {
            Console.WriteLine("Paint Rubber Duck");
        }

        public override void Quack() {
            Console.WriteLine("SQUEEEEZZZZ");
        }
    }
}