﻿using System;

namespace Ducks
{
    class Program
    {
        static void Main(string[] args)
        {
            var duck = new Duck();
            duck.Run();

            var mallardDuck = new MallardDuck();
            mallardDuck.Run();

            var rubberDuck = new RubberDuck();
            rubberDuck.Run();
        }
    }
}
