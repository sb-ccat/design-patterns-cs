using System;

namespace Ducks
{
    public class Duck
    {
        public void Swim() {
            Console.WriteLine("Swimming");
        }

        public void Fly() {
            Console.WriteLine("Flying");
        }

        public virtual void Quack() {
            Console.WriteLine("Quack");
        }

        public virtual void Display() {
            Console.WriteLine("Paint Duck");
        }

        public void Run() {
            Console.WriteLine();
            Console.WriteLine("====================");
            Display();
            Quack();
            Swim();
            Fly();
        }
    }
}