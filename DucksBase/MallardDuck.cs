using System;

namespace Ducks
{
    public class MallardDuck : Duck
    {

        public override void Display() {
            Console.WriteLine("Paint Mallard Duck");
        }
    }
}