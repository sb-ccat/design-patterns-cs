namespace GoogleFacebook
{
    public class FacebookUser
    {
        public string Name {get; set;}

        public FacebookUser(string name) {
            this.Name = name;
        }

        public override string ToString() {
            return "Facebook Username: " +  this.Name;
        }
    }
}