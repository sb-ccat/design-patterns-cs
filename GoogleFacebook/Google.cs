using System.Collections;
using System.Collections.Generic;

namespace GoogleFacebook
{
    public class Google : IEnumerable
    {
        private List<GoogleUser> users;
        
        public Google() {
            this.users = new List<GoogleUser>() {
                new GoogleUser("a@gmail.com"),
                new GoogleUser("b@gmail.com"),
                new GoogleUser("c@gmail.com"),
                new GoogleUser("d@gmail.com")
            };
        }
        public IEnumerator<GoogleUser> GetGoogleUsersEnumerator() {
            foreach (var item in this.users)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetGoogleUsersEnumerator();
        }
    }
}