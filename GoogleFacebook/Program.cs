﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GoogleFacebook
{
    class Program
    {
        static void Main(string[] args)
        {
            var companies = new List<IEnumerable>();
            companies.Add(new Google());
            companies.Add(new Facebook());
            companies.Add(new Facebook());
            companies.Add(new Google());

            foreach (var company in companies)
            {
                showData(company);
            }
        }

        static void showData(IEnumerable aggregate) {
            foreach (var item in aggregate)
            {
                Console.WriteLine(item);
            }
        }

        /* Problema con implementacion normal 
            // Facebook []
            for(i=...)
            // Google List
            foreach()
            // CRE Excel
            foreach (fileInput Excel);
        */
    }
}
