namespace GoogleFacebook
{
    public class GoogleUser
    {
        public string Email {get; set;}

        public override string ToString() {
            return "Google User: " +  this.Email;
        }

        public GoogleUser(string email) {
            this.Email = email;
        }
    }
}