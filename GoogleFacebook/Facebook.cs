using System.Collections;
using System.Collections.Generic;

namespace GoogleFacebook
{
    public class Facebook : IEnumerable
    {
        private FacebookUser[] users;
        
        public Facebook() {
            this.users = new FacebookUser[5];
            this.users[0] = new FacebookUser("f0");
            this.users[2] = new FacebookUser("f2");
            this.users[3] = new FacebookUser("f3");
        }
        public IEnumerator<FacebookUser> GetFacebookUserEnumerator() {
            foreach (var item in this.users)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetFacebookUserEnumerator();
        }
    }
}