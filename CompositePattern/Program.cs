﻿using System;

namespace CompositePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Composite Pattern (ls/dir)");

            Console.WriteLine("-----------------");
            Console.WriteLine("Archivo Simple LEAF");
            var word = new WordFile("manual");
            word.Show();

            Console.WriteLine("-----------------");
            Console.WriteLine("Folder Vacio COMPOSITE");
            var tmp = new Folder("tmp");
            tmp.Show();

            Console.WriteLine("-----------------");
            Console.WriteLine("Folder Documentos COMPOSITE");
            var docs = new Folder("Documentos");
            docs.AddFile(word);
            docs.Show();

            Console.WriteLine("-----------------");
            Console.WriteLine("Folder Images COMPOSITE");
            var images = new Folder("Images");
            images.AddFile(new AnyFile("wallpaper", "jpg"));
            images.AddFile(new AnyFile("picture", "gif"));
            images.AddFile(new AnyFile("nature", "png"));
            images.Show();

            Console.WriteLine("-----------------");
            Console.WriteLine("Folder COMPOSITE");
            var home = new Folder("Home");
            home.AddFile(tmp);
            home.AddFile(docs);
            home.AddFile(word);
            home.AddFile(images);
            home.Show();
        }
    }
}
