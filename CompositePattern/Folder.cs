using System;
using System.Collections.Generic;

namespace CompositePattern
{
    public class Folder : IFile // Composite
    {
        private string name;
        private List<IFile> files;

        public Folder(string name) {
            this.name = name;
            this.files = new List<IFile>();
        }

        public void AddFile(IFile file) {
            this.files.Add(file);
        }

        public void Show() // Ejecuto para mi y para mis hijos
        {
            Console.WriteLine("+ {0}", this.name);
            foreach (var file in this.files)
            {
                file.Show();
            }
        }
    }
}