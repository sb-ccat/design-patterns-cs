using System;

namespace CompositePattern
{
    public class WordFile : IFile
    {
        private string name;

        public WordFile(string name) {
            this.name = name;
        }

        public void Show()
        {
            Console.WriteLine("- {0}.docx", this.name);
        }
    }
}