using System;

namespace CompositePattern
{
    public class AnyFile : IFile
    {
        private string name;
        private string ext;

        public AnyFile(string name, string ext) {
            this.name = name;
            this.ext = ext;
        }
        public void Show()
        {
            Console.WriteLine("- {0}.{1}", this.name, this.ext);
        }
    }
}