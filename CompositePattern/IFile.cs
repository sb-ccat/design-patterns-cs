namespace CompositePattern
{
    public interface IFile // Component
    {
         void Show();
    }
}