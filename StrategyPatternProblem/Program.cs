﻿using System;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Non strategy Pattern");

            var team1 = new Team("Real America");
            var team2 = new Team("Oriente");

            team1.Opponent = team2;
            team2.Opponent = team1;

            team1.Play();
            team2.Play();

            team1.HasBall = true;
            //team2.HasBall = false;
            team1.Play();
            team2.Play();

            team1.HasBall = false;
            //team2.HasBall = true;
            team1.Play();
            team2.Play();
        }
    }
}
