using System;

namespace StrategyPattern
{
    public class Team
    {
        private string name;

        public Team opponent;
        public bool HasBall {get; set;}

        public Team(string name) {
            this.name = name;
            this.HasBall = false;
        }

        public Team Opponent { set => this.opponent = value; }

        public void Attack() {
            if (this.HasBall && !this.opponent.HasBall) {
                Console.WriteLine("Attack");
            } else {
                Console.WriteLine("None");
            }           
        }

        public void Defense() {
            if (!this.HasBall && this.opponent.HasBall) {
                Console.WriteLine("Defense");
            } else {
                Console.WriteLine("None");
            }
        }

        public void Play() {
            if (this.HasBall) this.Attack();
            else this.Defense();
        }
    }
}