using System.Collections.Generic;

namespace ObserverPattern
{
    public class Model
    {
        private int visits;
        private List<IObserverAlt> observers;

        public Model() {
            this.visits = 0;
            this.observers = new List<IObserverAlt>();
        }
        public int Visits {
            get => this.visits;
            set {
                this.visits = value;
                this.notifyAll();
            }
        }

        public void attach(IObserverAlt observer) {
            this.observers.Add(observer);
        }

        public void notifyAll() {
            foreach (var obs in observers)
            {
                obs.notify(this.Visits);
            }
        }
    }
}