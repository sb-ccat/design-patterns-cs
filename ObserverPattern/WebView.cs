using System;

namespace ObserverPattern
{

    
    public class WebView : IObserverAlt
    {
        public void notify(int value)
        {
            Console.WriteLine("----->");
            Console.WriteLine("New value in the Model {0}", value);
        }
    }
}