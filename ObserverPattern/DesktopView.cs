using System;

namespace ObserverPattern
{
    public class DesktopView : IObserverAlt
    {
        public void notify(int value)
        {
            Console.WriteLine("Desktop update to {0}", value);
        }
    }
}