namespace ObserverPattern
{
    public class Model
    {
        private int visits;
        public int Visits {
            get => this.visits;
            set { this.visits = value; }
        }

        public Model() {
            this.visits = 0;
        }
    }
}