﻿using System;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MVC example");

            var model = new Model();
            var webView = new WebView(model);
            webView.updateInfo();
            model.Visits = 5;
            model.Visits = 3;
            model.Visits = 1;
            model.Visits = 2;
            webView.updateInfo();
        }
    }
}
