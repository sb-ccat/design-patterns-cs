using System;

namespace ObserverPattern
{

    
    public class WebView
    {

        private Model model;
        private int oldValue;
        private int currentValue;

        public WebView(Model model) {
            this.model = model;
            this.oldValue = this.currentValue = model.Visits;
        }

        public void updateInfo() {
            if (this.currentValue != this.model.Visits) {
                Console.WriteLine("New changes");
                this.oldValue = this.currentValue;
                this.currentValue = this.model.Visits;
            }
        }
    }
}