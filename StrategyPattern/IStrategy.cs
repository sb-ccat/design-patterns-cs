namespace StrategyPattern
{
    public interface IStrategy
    {
         void Play(int score);
    }
}