using System;

namespace StrategyPattern
{
    public class WaitStrategy : IStrategy
    {
        public void Play(int score)
        {
            Console.WriteLine("Waiting...");
        }
    }
}