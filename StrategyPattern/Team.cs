using System;

namespace StrategyPattern
{
    public class Team
    {
        private string name;

        public IStrategy Strategy {get; set;}

        public int Score {get; set;}

        public Team(string name) {
            this.name = name;
            this.Score = 0;
            this.Strategy = new WaitStrategy();
        }

        public void Play() { // Puede ser cualquier nombre
            Console.Write("Equipo: {0} -> ", this.name);
            this.Strategy.Play(this.Score);
        }
    }
}