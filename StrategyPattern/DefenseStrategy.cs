using System;

namespace StrategyPattern
{
    public class DefenseStrategy : IStrategy
    {
        public void Play(int score)
        {
            if (score > 5) {
                Console.WriteLine("Defense");
            } else if (score > 1) {
                Console.WriteLine("Ultra Defense");
            } else {
                Console.WriteLine("Easy defense");
            }
        }
    }
}