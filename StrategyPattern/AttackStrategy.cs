using System;

namespace StrategyPattern
{
    public class AttackStrategy : IStrategy
    {
        public void Play(int score)
        {
            if (score > 5) {
                Console.WriteLine("Attack slow");
            } else if (score > 0) {
                Console.WriteLine("Attack");
            } else {
                Console.WriteLine("Crazy Attack");
            }
        }
    }
}