namespace SingletonPattern
{
    public class Singleton
    {
      private int hashCode;
      private static Singleton instance;
      private static volatile object flag = new object();

      static Singleton() {  }

      public static Singleton Instance {
        get {
          lock(flag) {
            if (instance == null) {
              lock (flag) {
                instance = new Singleton();
              }
            }
            return instance;
          }
        }
      }

      private Singleton() {
        this.hashCode = this.GetHashCode();
      }

      public override string ToString() {
        return "Singleton Code: " + this.hashCode;
      }
    }
}