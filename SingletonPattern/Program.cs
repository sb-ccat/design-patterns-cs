﻿using System;
using System.Threading;

namespace SingletonPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Singleton Pattern");

            for (int j = 0; j < 10000; j++)
            {
              for (int i = 0; i < 10; i++)
              {
                Console.WriteLine("Hilo No: {0}", i);
                var thread = new Thread(new ThreadStart(CreatePrintSingleton));
                thread.Start();
              }   
            }
        }

        static void CreatePrintSingleton() {
          var sg = Singleton.Instance;
          Console.WriteLine("SG: {0}", sg);
        }
    }
}
