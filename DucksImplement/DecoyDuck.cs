using System;

namespace Ducks
{
    public class DecoyDuck : Duck
    {

        public override void Display() {
            Console.WriteLine("Paint DECOY Duck");
        }
    }
}