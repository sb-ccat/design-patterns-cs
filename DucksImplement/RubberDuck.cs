using System;

namespace Ducks
{
    public class RubberDuck : Duck, IQuackable
    {
        public override void Display() {
            Console.WriteLine("Paint Rubber Duck");
        }

        public void Quack() {
            Console.WriteLine("SQUEEEEZZZZ");
        }

        public override void Run() {
            base.Run();
            Quack();
        }
    }
}