using System;

namespace Ducks
{
    public class Duck
    {
        public void Swim() {
            Console.WriteLine("Swimming");
        }

        public virtual void Display() {
            Console.WriteLine("Paint Duck");
        }

        public virtual void Run() {
            Console.WriteLine();
            Console.WriteLine("====================");
            Display();
            Swim();
        }
    }
}