using System;

namespace Ducks
{
    public class MallardDuck : Duck, IFlyable, IQuackable
    {

        public void Quack() {
            Console.WriteLine("Quack");
        }
        
        public override void Display() {
            Console.WriteLine("Paint Mallard Duck");
        }

        public void Fly()
        {
            Console.WriteLine("Flying");
        }

        public override void Run() {
            base.Run();
            Fly();
            Quack();
        }
    }
}