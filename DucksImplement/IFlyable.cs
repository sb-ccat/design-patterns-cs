namespace Ducks
{
    public interface IFlyable
    {
         void Fly();
    }
}