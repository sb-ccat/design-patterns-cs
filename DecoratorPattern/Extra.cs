namespace DecoratorPattern
{
    public class Extra : IFood // Decorator
    {
        private IFood food;
        private string name;

        public Extra(IFood food, string name) {
            this.food = food;
            this.name = name;
        }

        public string describe()
        {
            return this.food.describe() + " + " + this.name;
        }
    }
}
