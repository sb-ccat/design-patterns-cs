﻿using System;

namespace DecoratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Decorator Pattern");

            IFood pizza = new Pizza();
            Console.WriteLine(pizza.describe());

            pizza = new Tomato(pizza);
            Console.WriteLine(pizza.describe());

            pizza = new Extra(pizza, "Onion");
            pizza = new Extra(pizza, "Carne");
            pizza = new Extra(pizza, "Pepino");
            pizza = new Extra(pizza, "Papa Frita");

            Console.WriteLine(pizza.describe());
        }
    }
}
