namespace DecoratorPattern
{
    public class Pizza : IFood // ConcreteComponent
    {
        public string describe()
        {
            return "Pizza";
        }
    }
}