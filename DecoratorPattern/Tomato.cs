namespace DecoratorPattern
{
    public class Tomato : IFood
    {
        private IFood food;

        public Tomato (IFood food) {
            this.food = food;
        }

        public string describe()
        {
            return this.food.describe() + " + Tomato";
        }
    }
}