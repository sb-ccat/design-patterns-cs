namespace DecoratorPattern
{
    public interface IFood // Component
    {
        string describe();
    }
}