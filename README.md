# Patrones de diseño 3ra versión (CSharp) #

## Diapositivas guia ##
[Enlace a Google Doc](https://docs.google.com/presentation/d/1-kT1bwr8P7rMQKINNZQZcLnRrsrpYfyWRxwnrwHvj80/edit?usp=sharing)


## Software a instalar ##
1. [.NET Core](https://microsoft.com/net/core)
2. [Visual Studio Code](https://code.visualstudio.com/)

Alternativa:
[Visual Studio Community](https://www.visualstudio.com/vs/community/)


### Visual Studio Code + C# ###
- [Guía de instalación](https://code.visualstudio.com/docs/languages/csharp)


## Contenido del Curso ##
1. Diseño orientado a objetos: Aspectos fundamentales
2. Introducción a Patrones de Diseño
3. Patrones de comportamiento (Iterator, Strategy y Observer)
4. Patrones estructurales (Composite y Decorator)
5. Patrones creacionales (Singleton)


### Problemas propuestos ###
1. Google y Facebook (Iterator)
2. Gilded Rose (Strategy) [Enlace](https://github.com/emilybache/GildedRose-Refactoring-Kata/tree/master/csharp)
3. Vistas Reactivas (Observer)
4. Listar Directorios (Composite)
5. Preparame una pizza (Decorator)



## Bibliografía ##
- Freeman, E.; Robson, E.; Sierra, K. & Bates, B. (2004). *Head First Design Patterns*. O'Reilly.
- Gamma, E.; Vlissides, J.; Johnson, R. & Helm, R. (1994). *Design Patterns: Elements of Reusable Object-Oriented Software*. Addison-Wesley.
- Horstmann, C. (2005). *OO Design & Patterns, 2nd ed*. Wiley Publisher.

Archivo PDF con contenido del libro base del curso [aquí](https://drive.google.com/open?id=0B9zdeJdLzq8reVBqa2labE5pb0U)

### Siguientes patrones ###
- Abstract Factory
- Memento
- Chain of Responsability
- Facade
- Adapter
