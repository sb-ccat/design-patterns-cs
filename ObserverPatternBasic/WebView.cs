using System;

namespace ObserverPattern
{

    
    public class WebView : IObserverAlt
    {
        public void notify()
        {
            Console.WriteLine("----->");
            Console.WriteLine("New Changes in the Model");
        }
    }
}