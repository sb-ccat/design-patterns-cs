﻿using System;

namespace IteratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iterator Pattern Default C#");

            var bank = new Bank();

            foreach (var client in bank)
            {
                Console.WriteLine(client);
            }
        }
    }
}
