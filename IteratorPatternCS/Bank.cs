using System.Collections.Generic;

namespace IteratorPattern
{
    public class Bank
    {
        private List<string> clients;

        public Bank() {
            this.clients = new List<string>() {
                "Roberto", "Madepa", "Fino", "Daniel"
            };
        }

        public IEnumerator<string> GetEnumerator() {
            foreach (var item in this.clients)
            {
                yield return item;
            }
        }
    }
}