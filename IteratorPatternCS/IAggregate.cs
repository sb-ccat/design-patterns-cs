namespace IteratorPattern
{
    public interface IAggregate
    {
         IteratorInterface createIterator();
    }
}